const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');

const plugins = [
	new HtmlWebpackPlugin({
		template: './public/index.html',
		filename: './index.html'
	}),
	new webpack.HotModuleReplacementPlugin()
];

module.exports = {
	entry: [ 'babel-polyfill', './src/index.js' ],
	output: {
		path: __dirname + '/dist',
		publicPath: '/',
		filename: 'bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader'
				}
			},
			{
				test: /\.scss$/,
				use: [ 'style-loader', 'css-loader', 'sass-loader' ]
			}
		]
	},
	resolve: {
		modules: [ path.resolve(__dirname, './src'), 'node_modules' ],
		extensions: [ '*', '.js', '.jsx', '.scss' ],
		alias: {
			reducers: path.resolve(__dirname, './src')
		}
	},
	devServer: {
		contentBase: './dist',
		historyApiFallback: true,
		hot: true,
		port: 8080
	},
	plugins
};
