import { createStore, applyMiddleware, compose } from 'redux';
import { rootReducer } from '../reducers/main';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../sagas/todo';

const sagaMiddleware = createSagaMiddleware();

export default createStore(
	rootReducer,
	compose(
		applyMiddleware(sagaMiddleware),
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	)
);

sagaMiddleware.run(rootSaga);