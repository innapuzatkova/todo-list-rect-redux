import {
  ADD_TODO,
  GET_TODOLIST_FULL,
  TODOLIST_RECEIVED,
  EDIT_TODO,
  DELETE_TODO,
  TOGGLE_TODO,
  TODO_TOGGLED,
  TODO_ADDED,
  TODO_UPDATED,
  TODO_DELETED
} from '../constants/actionTypes';

export const addTodo = (todo) => ({
  type: ADD_TODO,
  payload: {
    ...todo,
    completed: false
  }
});

export const getTodoListFull = () => ({
  type: GET_TODOLIST_FULL
});

export const todoListReceived = (data) => ({
  type: TODOLIST_RECEIVED,
  data: data
});

export const deleteTodo = (id) => ({
  type: DELETE_TODO,
  id
});

export const toggleTodo = (id, isCompleted) => ({
  type: TOGGLE_TODO,
  id,
  isCompleted: {
    completed: isCompleted
  }
});

export const todoToggled = (todo) => ({
  type: TODO_TOGGLED,
  id: todo.id,
  completed: todo.completed
});


export const editTodo = (todoData, todoId) => ({
  type: EDIT_TODO,
  payload: todoData,
  id: todoId
});

export const todoAdded = (todo) => ({
  type: TODO_ADDED,
  data: todo
})

export const todoUpdated = (todo) => ({
  type: TODO_UPDATED,
  data: todo
})

export const todoDeleted = (id) => ({
  type: TODO_DELETED,
  id: id
})