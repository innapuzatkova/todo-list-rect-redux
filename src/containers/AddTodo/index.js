import React from 'react';
import { connect } from 'react-redux';
import { reset } from 'redux-form';

import TodoForm from 'components/TodoForm';
import { addTodo } from 'actions/todos';

class AddTodo extends React.Component {
	onSubmitHandler = (values) => {
		this.props.addTodo(values);
		this.props.resetForm();
	};
	render() {
		return <TodoForm onSubmit={this.onSubmitHandler} btnText="Add todo" form="addTodo" btnStyle="btn-success" />;
	}
}

const mapDispatchToProps = {
	addTodo: (values) =>
		addTodo({
			title: values.title,
			description: values.description
		}),
	resetForm: () => reset('addTodo')
};

export default connect(null, mapDispatchToProps)(AddTodo);
