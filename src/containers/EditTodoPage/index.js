import React from 'react';
import {connect} from 'react-redux';
import TodoForm from 'components/TodoForm';
import {getTodoById} from "selectors/todoSelectors";
import {editTodo} from "actions/todos";

let EditTodoPage = ({editTodo, values, todoId, initialValues}) =>
    <TodoForm
        onSubmit={(values) => editTodo(values, todoId)}
        btnText="Edit todo"
        initialValues={initialValues}
        form="editTodo"
        todoId={todoId}
        btnStyle="btn-basic"/>;

const mapStateToProps = (state, owProps) => {
  const todo = getTodoById(state, owProps.match.params.id);
  return {
    todoId: todo.id,
    initialValues: {
      title: todo.title,
      description: todo.description
    }
  }
};

const mapDispatchToProps = {
  editTodo: (values, id) =>
      editTodo({
        title: values.title,
        description: values.description
      }, id)
};

export default connect(mapStateToProps, mapDispatchToProps)(EditTodoPage);