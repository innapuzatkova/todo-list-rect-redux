import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import TodoList from 'components/TodoList';
import AddTodo from 'containers/AddTodo';

import { getTodoListFull, deleteTodo, toggleTodo } from 'actions/todos';
import {getTodos} from 'selectors/todoSelectors';

class App extends Component {
	componentDidMount() {
		this.props.getTodoListFull();
	}

	render() {
		return (
			<Fragment>
				<AddTodo />
				<TodoList
					className="app-todo-list"
					todos={this.props.todos}
					deleteHandler={this.props.deleteTodo}
					todoToggleHandler={this.props.toggleTodo}
				/>
			</Fragment>
		);
	}
}

const mapStateToProps = (state, props) => {
	return {
		todos: getTodos(state, props)
	};
};

const mapDispatchToProps = {
	getTodoListFull,
	deleteTodo: (id) => deleteTodo(id),
	toggleTodo: (id, isCompleted) => toggleTodo(id, isCompleted)

};

export default connect(mapStateToProps, mapDispatchToProps)(App);
