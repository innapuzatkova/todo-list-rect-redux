import React from 'react';
import ReactDOM from 'react-dom';
import store from './store/store';

import 'styles/style';

import Root from './components/Root/index';

	ReactDOM.render(
		<Root store={store}/>,
		document.getElementById('root')
	);