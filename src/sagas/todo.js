import { put, takeLatest, all, fork, call } from 'redux-saga/effects';
import { Api } from '../api/Api';
import { ADD_TODO, GET_TODOLIST_FULL, DELETE_TODO, TOGGLE_TODO, EDIT_TODO, TODO_ADDED } from '../constants/actionTypes';
import { todoListReceived, todoToggled, todoAdded, todoUpdated, todoDeleted } from '../actions/todos';

function* fetchTodos() {
	try {
		const data = yield call(Api.get, '/todos');
		yield put(todoListReceived(data));
	} catch (e) {
		throw e.message;
	}
}

function* createTodo(action) {
	try {
		const addedTodo = yield call(Api.post, '/todos', action.payload);
		yield put(todoAdded(addedTodo));
	} catch (e) {
		throw e.message;
	}
}

function* deleteTodo(action) {
	try {
		yield call(Api.delete, `/todos/${action.id}`);
		yield put(todoDeleted(action.id));
	} catch (e) {
		throw e.message;
	}
}

function* toggleTodo(action) {
	try {
		const todoUpdated = yield call(Api.patch, `/todos/${action.id}`, action.isCompleted);
		yield put(todoToggled(todoUpdated));
	} catch (e) {
		throw e.message;
	}
}

function* editTodo(action) {
	try {
		const updatedTodo = yield call(Api.patch, `/todos/${action.id}`, action.payload);
		yield put(todoUpdated(updatedTodo));
	} catch (e) {
		throw e.message;
	}
}

export default function* rootSaga() {
	yield all([
		takeLatest(GET_TODOLIST_FULL, fetchTodos),
		takeLatest(ADD_TODO, createTodo),
		takeLatest(DELETE_TODO, deleteTodo),
		takeLatest(TOGGLE_TODO, toggleTodo),
		takeLatest(EDIT_TODO, editTodo)
	]);
}
