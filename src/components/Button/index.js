import React from 'react';

import './index.scss';

let Button=({btnStyle, btnText, onClick, disabled})=>(
   <button onClick={onClick} className={btnStyle} disabled={disabled}>{btnText}</button>
);

export default Button;