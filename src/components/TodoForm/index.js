import React, { Fragment } from 'react';
import { Field, reduxForm } from 'redux-form';
import Button from 'components/Button';
import { connect } from 'react-redux';
import './index.scss';

class TodoForm extends React.Component {
	
	renderField = ({input, className, meta: {touched, error}}) => (
		<Fragment>
			<label className="field__label" htmlFor={input.name}>{`${input.name}:`}</label>{' '}
			<input
				{...input}
				className={`${className} ${touched && error && 'input--invalid'}`}
				type="text"
			/>
			{touched && error && <span className="error__message">{error}</span>}
		</Fragment>
	);

	render() {
		const { handleChange, handleSubmit, value, btnText, btnStyle, pristine, valid } = this.props;
		return (
			<form onSubmit={handleSubmit}>
				<Field
					className="input__field"
					name="title"
					component={this.renderField}
					type="text"
					value={value}
					onChange={handleChange}
				/>{' '}
				<Field
					className="input__field"
					name="description"
					component={this.renderField}
					type="text"
					value={value}
					onChange={handleChange}
				/>{' '}
				<Button type="submit" btnStyle={btnStyle} btnText={btnText} disabled={!valid || pristine} />
			</form>
		);
	}
}

const validate = (values) => {
	const errors = {};
	if (!values.title) {
		errors.title = '*Required';
	} else if (!/^[a-zA-Z0-9_.-@]*$/.test(values.title)) {
		errors.title = '*Invalid title'
	}
	if (!values.description) {
		errors.description = '*Required';
	} else if (!/^[a-zA-Z0-9_.-@]*$/.test(values.description)) {
		errors.description = '*Invalid description'
	}
	return errors;
};

const mapStateToProps = (state, props) => {
	if (props.form === 'addTodo') {
		return {};
	}
	return { initialValues: props.initialValues, todoId: props.todoId };
};

export default connect(mapStateToProps)(
	reduxForm({
		form: 'todoForm',
		validate,
		enableReinitialize: true
	})(TodoForm)
);
