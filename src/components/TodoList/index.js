import React from 'react';
import TodoItem from 'components/TodoItem';

import './index.scss';

const TodoList = ({ todos, className, deleteHandler, todoToggleHandler }) => {
	return (
		<ul className={`${className} todo-list`}>
			{todos.map((todo) => {
				return (
					<li key={todo.id} className="todo-list__item">
						<TodoItem todo={todo} deleteHandler={deleteHandler} todoToggleHandler={todoToggleHandler}/>
					</li>
				);
			})}
		</ul>
	);
};
export default TodoList;