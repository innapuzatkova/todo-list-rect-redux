import React from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'


import App from 'containers/App';
import EditTodoPage from 'containers/EditTodoPage'

const Root = ({store}) => (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/" component={App}/>
          <Route path="/edit/:id" component={EditTodoPage}/>
        </Switch>
      </Router>
    </Provider>
);

export default Root;