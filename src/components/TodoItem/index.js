import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import Button from 'components/Button';

import './index.scss';

export default class TodoItem extends React.Component {
  render() {
    const {todo, deleteHandler, todoToggleHandler} = this.props;
    const isCompleted = todo.completed;
    return (
        <Fragment>
          <h4 className={`todo__title ${isCompleted && 'todo--completed' }`}>{todo.title}</h4>
          <p className={`${isCompleted && 'todo--completed' }`}>{todo.description}</p>
          <div className="todo__controls">
            <div className="todo__buttons">
              <Link to={`/edit/${todo.id}`} type="submit" className="edit__link">Edit todo</Link>
              <Button type="submit" onClick={() => deleteHandler(todo.id)} btnStyle="btn-danger" btnText="Delete"/> {''}
            </div>
            <div className="toggle">
              <label htmlFor="todo-complete">Done</label>
              <input type="checkbox" className="todo__toggle" name="todo-complete" checked={isCompleted}
                     onChange={() => todoToggleHandler(todo.id, !isCompleted)}/>
            </div>
          </div>
        </Fragment>
    );
  }
};


