import { createSelector } from 'reselect';

export const getTodos = (state, props) => state.todos;

export const getTodoId = (state, id) => id;


export const getTodoById = createSelector([getTodos, getTodoId], (todos, id) => {
  return todos.find((todo) => {
    return todo.id === +id;
  });
});
