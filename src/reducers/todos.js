import { TODOLIST_RECEIVED, TODO_TOGGLED, TODO_ADDED, TODO_UPDATED, TODO_DELETED } from '../constants/actionTypes';

export const todos = (state = [], action) => {
	switch (action.type) {
		case TODOLIST_RECEIVED:
			return [ ...action.data ];
		case TODO_TOGGLED:
			return state.map((t) => todo(t, action));
		case TODO_ADDED:
			return [ ...state, todo(undefined, action) ];
		case TODO_UPDATED:
			return state.map((t) => todo(t, action));
		case TODO_DELETED:
			return state.filter((t) => todo(t, action));
		default:
			return state;
	}
};

export const todo = (state = {}, action) => {
	switch (action.type) {
		case TODO_TOGGLED:
			if (state.id !== action.id) {
				return state;
			}
			return {
				...state,
				completed: action.completed
			};
		case TODO_ADDED:
			return {
				...action.data
			};
		case TODO_UPDATED:
			if (state.id !== action.id) {
				return state;
			}
			return {
				...state,
				...action.data
			};
		case TODO_DELETED:
			return state.id !== action.id;
		default:
			return state;
	}
};
