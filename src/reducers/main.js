import React from 'react';
import { combineReducers } from 'redux';
import { todos } from './todos';
import { reducer as formReducer } from 'redux-form';

export const rootReducer = combineReducers({
    todos,
    form: formReducer
});
